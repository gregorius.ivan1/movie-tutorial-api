﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace MovieAPITutorial.Migrations
{
    /// <inheritdoc />
    public partial class SeedMovieTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Actors", "Details", "Genre", "Images", "Rating", "Title", "Year" },
                values: new object[,]
                {
                    { 1, "[\"Tim Robbins\",\"Morgan Freeman\"]", "", "[3]", "[]", 9.3f, "The Shawshank Redemption", 1994 },
                    { 2, "[\"Marlon Brando\",\"Al Pacino\"]", "", "[13,3]", "[]", 9.2f, "The Godfather", 1972 },
                    { 3, "[\"Christian Bale\",\"Heath Ledger\"]", "", "[0,13,3]", "[]", 9f, "The Dark Knight", 2008 },
                    { 4, "[\"John Travolta\",\"Uma Thurman\"]", "", "[13,3]", "[]", 8.9f, "Pulp Fiction", 1994 },
                    { 5, "[\"Liam Neeson\",\"Ralph Fiennes\"]", "", "[20,3,15]", "[]", 8.9f, "Schindler's List", 1993 },
                    { 6, "[\"Elijah Wood\",\"Viggo Mortensen\"]", "", "[1,3,4]", "[]", 8.9f, "The Lord of the Rings: The Return of the King", 2003 },
                    { 7, "[\"Tom Hanks\",\"Robin Wright\"]", "", "[3,7]", "[]", 8.8f, "Forrest Gump", 1994 },
                    { 8, "[\"Leonardo DiCaprio\",\"Joseph Gordon-Levitt\"]", "", "[0,1,8]", "[]", 8.8f, "Inception", 2010 },
                    { 9, "[\"Keanu Reeves\",\"Laurence Fishburne\"]", "", "[0,8]", "[]", 8.7f, "The Matrix", 1999 },
                    { 10, "[\"Jodie Foster\",\"Anthony Hopkins\"]", "", "[13,3,9]", "[]", 8.6f, "The Silence of the Lambs", 1991 }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 10);
        }
    }
}
