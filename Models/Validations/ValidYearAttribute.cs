﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace MovieAPITutorial.Models.Validations
{
    public sealed class ValidYearAttribute : ValidationAttribute
    {
        private readonly int _minYear = 1800;
        private readonly int _maxYear = 9999;
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            if (!int.TryParse(value.ToString(), out int year))
            {
                return new ValidationResult("Invalid year format.");
            }

            if (year < _minYear || year > _maxYear)
            {
                return new ValidationResult($"Year must be between {_minYear} and {_maxYear}.");
            }

            return ValidationResult.Success;
        }
    }
}
