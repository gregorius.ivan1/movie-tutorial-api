﻿using MovieAPITutorial.Models.Validations;
using System.ComponentModel.DataAnnotations;

namespace MovieAPITutorial.Models.DTO
{
    public class MoviesDTO
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public required string Title { get; set; }
        [Required]

        [ValidYear]
        public int Year { get; set; }
        public string[] Genre { get; set; } = [];
    }
}
