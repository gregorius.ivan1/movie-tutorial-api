﻿using System.ComponentModel.DataAnnotations;

namespace MovieAPITutorial.Models.DTO
{
    public class LoginUserDTO
    {
        [Required]
        public required string Username { get; set; }
        [Required]
        public required string Password { get; set; }
    }
}
