﻿using Microsoft.EntityFrameworkCore;
using MovieAPITutorial.Data;
using MovieAPITutorial.Models.Validations;
using System.ComponentModel.DataAnnotations;

namespace MovieAPITutorial.Models.Model
{
    [Index(nameof(Title), IsUnique = true)]
    public class Movies
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [Unique<Movies>(typeof(MovieAPIContext))]
        public required string Title { get; set; }
        [Required]

        [ValidYear]
        public int Year { get; set; }
        public string[] Genre { get; set; } = [];
        public string[] Actors { get; set; } = [];

        [Range(0.0, 10.0)]
        public float Rating { get; set; } = 0;
        public string[] Images { get; set; } = [];
        public string Details { get; set; } = string.Empty;
    }
}

