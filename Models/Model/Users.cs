﻿using Azure.Core;
using MovieAPITutorial.Models.DTO;

namespace MovieAPITutorial.Models.Model
{
    public class Users
    {
        public int Id { get; set; }
        public string Username { get; set; } = string.Empty;
        public string Fullname { get; set; } = string.Empty;
        public string PasswordHash { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string PhoneNumber { get; set; } = string.Empty;
        public Users() {}
        public Users(RegisterUserDTO request)
        {
            Username = request.Username;
            Fullname = request.Fullname;
            Email = request.Email;
            PhoneNumber = request.PhoneNumber;
            PasswordHash = GetPasswordHash();
        }

        private string GetPasswordHash()
        {
            return BCrypt.Net.BCrypt.HashPassword(PasswordHash);
        }
    }
}
