﻿namespace MovieAPITutorial.Utilities
{
    public class GenreEnum
    {
        public enum Genre
        {
            Action,
            Adventure,
            Comedy,
            Drama,
            Fantasy,
            Horror,
            Mystery,
            Romance,
            SciFi,
            Thriller,
            Western,
            Documentary,
            Animation,
            Crime,
            Family,
            History,
            Music,
            War,
            Sport,
            Superhero,
            Biography
        }


    }
}
