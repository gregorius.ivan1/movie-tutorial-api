﻿using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using MovieAPITutorial.Data;
using MovieAPITutorial.Models.Model;

namespace MovieAPITutorial.Utilities.Filters
{
    public class MoviesFilter
    {
        public string? Title { get; set; }
        public string[]? Genre { get; set; }
        public int? Year { get; set; }

    }

    public class MovieFilterService
    {
        private readonly MovieAPIContext _context;

        public MovieFilterService(MovieAPIContext context)
        {
            _context = context;
        }
        public IQueryable<Movies> FilterMovies(MoviesFilter filter)
        {
            IQueryable<Movies> query = _context.Movies;

            if (!string.IsNullOrEmpty(filter?.Title))
            {
                query = query.Where(m => m.Title.Contains(filter.Title));
            }

            if (!filter.Genre.IsNullOrEmpty())
            {
                query = filter.Genre.Aggregate(query,
                    (currentQuery, filteredGenre) => currentQuery.Where(m => m.Genre.Contains(filteredGenre)));
            }

            if (filter.Year.ToString() != "")
            {
                query = query.Where(m => m.Year == filter.Year);
            }

            return query;
        }
    }
}
