﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MovieAPITutorial.Models.Model;
using static MovieAPITutorial.Utilities.GenreEnum;

namespace MovieAPITutorial.Data
{
    public class MovieAPIContext : DbContext
    {
        public MovieAPIContext (DbContextOptions<MovieAPIContext> options)
            : base(options)
        {
        }

        public DbSet<Movies> Movies { get; set; } = default!;
        public DbSet<Users> Users { get; set; } = default!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movies>().HasData(
                new Movies {
                    Id = 1,
                    Title = "The Shawshank Redemption",
                    Year = 1994,
                    Genre = new Genre[] { Genre.Drama }.Select(g => g.ToString()).ToArray(),
                    Actors = ["Tim Robbins", "Morgan Freeman"],
                    Rating = 9.3f
                },
                new Movies {
                    Id = 2,
                    Title = "The Godfather",
                    Year = 1972,
                    Genre = new Genre[] { Genre.Crime, Genre.Drama }.Select(g => g.ToString()).ToArray(),
                    Actors = ["Marlon Brando", "Al Pacino"],
                    Rating = 9.2f
                },
                new Movies {
                    Id = 3,
                    Title = "The Dark Knight",
                    Year = 2008,
                    Genre = new Genre[] { Genre.Action, Genre.Crime, Genre.Drama }.Select(g => g.ToString()).ToArray(),
                    Actors = new string[] { "Christian Bale", "Heath Ledger" },
                    Rating = 9.0f
                },
                new Movies { Id = 4,
                    Title = "Pulp Fiction",
                    Year = 1994,
                    Genre = new Genre[] { Genre.Crime, Genre.Drama }.Select(g => g.ToString()).ToArray(),
                    Actors = new string[] { "John Travolta", "Uma Thurman" },
                    Rating = 8.9f
                },
                new Movies {
                    Id = 5,
                    Title = "Schindler's List",
                    Year = 1993,
                    Genre = new Genre[] { Genre.Biography, Genre.Drama, Genre.History }.Select(g => g.ToString()).ToArray(),
                    Actors = ["Liam Neeson", "Ralph Fiennes"],
                    Rating = 8.9f
                },
                new Movies {
                    Id = 6,
                    Title = "The Lord of the Rings: The Return of the King",
                    Year = 2003, Genre = new Genre[] { Genre.Adventure, Genre.Drama, Genre.Fantasy }.Select(g => g.ToString()).ToArray(),
                    Actors = ["Elijah Wood", "Viggo Mortensen"],
                    Rating = 8.9f
                },
                new Movies { Id = 7,
                    Title = "Forrest Gump",
                    Year = 1994,
                    Genre = new Genre[] { Genre.Drama, Genre.Romance }.Select(g => g.ToString()).ToArray(),
                    Actors = ["Tom Hanks", "Robin Wright"],
                    Rating = 8.8f 
                },
                new Movies { Id = 8,
                    Title = "Inception",
                    Year = 2010, 
                    Genre = new Genre[] { Genre.Action, Genre.Adventure, Genre.SciFi }.Select(g => g.ToString()).ToArray(),
                    Actors = ["Leonardo DiCaprio", "Joseph Gordon-Levitt"],
                    Rating = 8.8f
                },
                new Movies { 
                    Id = 9,
                    Title = "The Matrix",
                    Year = 1999,
                    Genre = new Genre[] { Genre.Action, Genre.SciFi }.Select(g => g.ToString()).ToArray(),
                    Actors = ["Keanu Reeves", "Laurence Fishburne"],
                    Rating = 8.7f 
                },
                new Movies { 
                    Id = 10,
                    Title = "The Silence of the Lambs", 
                    Year = 1991,
                    Genre = new Genre[] { Genre.Crime, Genre.Drama, Genre.Thriller }.Select(g => g.ToString()).ToArray(),
                    Actors = ["Jodie Foster", "Anthony Hopkins"],
                    Rating = 8.6f 
                }
            );
        }
    }
}
