﻿using Asp.Versioning;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using MovieAPITutorial.Data;
using MovieAPITutorial.Models.DTO;
using MovieAPITutorial.Models.Model;
using NuGet.Protocol.Plugins;
using System.Collections.Immutable;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace MovieAPITutorial.Controllers
{
    [ApiVersion(1)]
    [Route("api/v{v:ApiVersion}/[controller]")]
    [ApiController]
    public class AuthController(MovieAPIContext context, IConfiguration configuration) : ControllerBase
    {
        private readonly MovieAPIContext _context = context;
        private readonly IConfiguration _configuration = configuration;

        [HttpGet, Authorize]
        public async Task<ActionResult<Users>> GetUsers()
        {
            List<Users> users = await _context.Users.ToListAsync();

            if(!users.Any())
            {
                return NotFound();
            }
            
            return Ok(users);

        }

        [HttpPost("Register")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult<Users>> Register(RegisterUserDTO request)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Users newUser = new Users(request);
            await _context.Users.AddAsync(newUser);
            await _context.SaveChangesAsync();

            return Ok(newUser);
        }

        [HttpPost("Login")]
        public async Task<ActionResult<Users>> Login(LoginUserDTO request)
        {
            Users? user = await _context.Users.FirstOrDefaultAsync(u => u.Username == request.Username);
            
            if (user == null)
            {
                return NotFound($"User {request.Username} Not Found");
            }
            if(!VerifyPassword(user, request.Password))
            {
                return Unauthorized("Invalid username or password"); 
            }

            return Ok(CreateToken(user));

        }

        private static bool VerifyPassword(Users user, string password) =>
            BCrypt.Net.BCrypt.Verify(password, user.PasswordHash);

        private string CreateToken(Users user)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.Username),
            };

            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
                _configuration.GetSection("AppSettings:Token").Value!));

            SigningCredentials credential = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            JwtSecurityToken token = new (
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: credential
                );

            string jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt;
        }

    }
}
