﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Asp.Versioning;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Microsoft.IdentityModel.Tokens;
using MovieAPITutorial.Data;
using MovieAPITutorial.Models.DTO;
using MovieAPITutorial.Models.Model;
using MovieAPITutorial.Utilities.Filters;

namespace MovieAPITutorial.Controllers
{
    [ApiVersion(1)]
    [Route("api/v{v:ApiVersion}/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieAPIContext _context;
        private readonly MovieFilterService _filterService;
        public MoviesController(MovieAPIContext context, MovieFilterService filterService)
        {
            _context = context;
            _filterService = filterService;
        }

        // GET: api/Movies
        [HttpGet()]
        public async Task<ActionResult<IEnumerable<Movies>>> GetMovies([FromQuery, Bind("Title","Genre[]","Year")] MoviesFilter filter)
        {

            IQueryable<Movies> query = _filterService.FilterMovies(filter);

            List<Movies> movies = await query.ToListAsync();

            if(!movies.Any())
            {
                return NotFound();
            }

            return Ok(movies);
        } 

        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Movies>> GetMovies(int id)
        {
            var movies = await _context.Movies.FindAsync(id);

            if (movies == null)
            {
                return NotFound();
            }

            return movies;
        }

        // PUT: api/Movies/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> EditMovies(int id, Movies movies)
        {
            if (id != movies.Id)
            {
                return BadRequest();
            }

            _context.Entry(movies).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MoviesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(new { message = $"Movie {movies.Id} updated successfully" }); ;
        }

        // POST: api/Movies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Movies>> PostMovies(Movies movies)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
                _context.Movies.Add(movies);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetMovies", new { id = movies.Id }, movies);
        }

        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovies(int id)
        {
            var movies = await _context.Movies.FindAsync(id);
            if (movies == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movies);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        private bool MoviesExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }

       private static MoviesDTO MoviesToDTO(Movies movies) => 
           new MoviesDTO
           {
               Id = movies.Id,
               Title = movies.Title,
               Year = movies.Year,
               Genre =  movies.Genre,
           };
    }
}
